FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
EXPOSE 8080:8080
EXPOSE 8084:8084
ADD /target/compra-0.0.1.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]