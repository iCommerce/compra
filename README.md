Contrato Compras

produto 8080
carrinho 8081
cliente 8082
cartao 8083
comprar 8084

Path: /comprar
Method: POST
BODY
/* Carrinho */
	private String idSessao;
/* Cliente */
	private String nome;
	private String telefone;
	private String email;
/* Cartao */
	private String numeroCartao;
	private String nomeClienteCartao;
	private String codigoSeguranca;
	private String email;
	private int dataValidade; //AAMM


Exemplo
{
	"idSessao": "#ID#SESSAO#CODIGO#HASH",
	"cliente":{ 
		"nome": "Linao",
		"telefone": "001",
		"email": "email@com.br"
	},
	"cartao": {
		"numeroCartao": "5999499949994999",
		"nomeClienteCartao": "Linao",
		"codigoSeguranca": "001",
		"dataValidade": 20180508
	}
}


Resposta: 200 - OK ou 400 - Bad Request