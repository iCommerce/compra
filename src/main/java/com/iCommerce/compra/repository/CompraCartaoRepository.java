package com.iCommerce.compra.repository;

import org.springframework.data.repository.CrudRepository;

import com.iCommerce.compra.model.CompraCartao;

public interface CompraCartaoRepository extends CrudRepository<CompraCartao, Integer> {

}

