package com.iCommerce.compra.repository;

import org.springframework.data.repository.CrudRepository;

import com.iCommerce.compra.model.CompraProduto;

public interface CompraProdutoRepository extends CrudRepository<CompraProduto, Integer> {

}
