package com.iCommerce.compra.repository;

import org.springframework.data.repository.CrudRepository;

import com.iCommerce.compra.model.CompraCliente;

public interface CompraClienteRepository extends CrudRepository<CompraCliente, Integer> {

}
