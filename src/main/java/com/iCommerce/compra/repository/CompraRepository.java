package com.iCommerce.compra.repository;

import org.springframework.data.repository.CrudRepository;

import com.iCommerce.compra.model.Compra;

public interface CompraRepository extends CrudRepository<Compra, Integer> {

}
