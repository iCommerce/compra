package com.iCommerce.compra.controller;


import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.iCommerce.compra.contracts.CarrinhoContract;
import com.iCommerce.compra.contracts.CartaoContract;
import com.iCommerce.compra.contracts.CartaoContratoResposta;
import com.iCommerce.compra.contracts.ClienteContract;
import com.iCommerce.compra.contracts.CompraContract;
import com.iCommerce.compra.model.Compra;
import com.iCommerce.compra.model.CompraCartao;
import com.iCommerce.compra.model.CompraCliente;
import com.iCommerce.compra.model.CompraProduto;
import com.iCommerce.compra.repository.CompraCartaoRepository;
import com.iCommerce.compra.repository.CompraClienteRepository;
import com.iCommerce.compra.repository.CompraProdutoRepository;
import com.iCommerce.compra.repository.CompraRepository;

@Controller
public class CompraController {

	//declaracao dos Repositories
	@Autowired
	CompraClienteRepository clienteRepository;
	@Autowired
	CompraRepository compraRepository;
	@Autowired
	CompraCartaoRepository cartaoRepository;
	@Autowired
	CompraProdutoRepository produtoRepository;


	@RequestMapping(path="/comprar", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> buscarAtividadeDia(@RequestBody CompraContract compraContract) {		

		try {
			//acesso as APIs
			RestTemplate restTemplate = new RestTemplate();
			//    ClienteContract cliente = restTemplate.getForObject("http://localhost:8081/cliente", ClienteContract.class);

			ClienteContract cliente = restTemplate.postForObject("http://104.131.164.162:8082/cliente", compraContract.getCliente(), ClienteContract.class);

			CarrinhoContract carrinho = restTemplate.getForObject("http://104.131.164.162:8083/carrinho/" + compraContract.getIdSessao(), CarrinhoContract.class);
			compraContract.atualizaValorCartao(carrinho.getValorTotal());

			CartaoContratoResposta respostaCartao = restTemplate.postForObject("http://104.131.164.162:8081/autorizacao", compraContract.getCartao(), CartaoContratoResposta.class);

			//insert na base de Compras
			Compra compra = new Compra();
			compra.setIdPedido(compraContract.getIdSessao());
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");  //gerando a data no formato timestamp
			compra.setIdCompra(dateFormat.format(new Date()));
			if (respostaCartao.getCodigoResposta() != 0) {
				compra.setStatusCompra("NOK");
			} else
			{
				compra.setStatusCompra("OK");

			}
			compra.setValorTotalCompra(carrinho.getValorTotal());
			compra = compraRepository.save(compra);

			//insert na base de CompraClientes
			CompraCliente compraCliente = new CompraCliente();
			ClienteContract clienteContract = compraContract.getCliente();
			compraCliente.setIdCompra(compra.getIdCompra());
			compraCliente.setIdCliente(cliente.getId());
			compraCliente.setEmail(clienteContract.getEmail());
			compraCliente.setNome(clienteContract.getNome());
			compraCliente.setTelefone(clienteContract.getTelefone());
			compraCliente.setCompra(compra);
			compraCliente = clienteRepository.save(compraCliente);

			//insert na base de Compra Cartao
			CompraCartao compraCartao = new CompraCartao();
			CartaoContract cartaoContract = compraContract.getCartao();
			compraCartao.setIdCompra(compra.getIdCompra());
			compraCartao.setNomeTitular(cartaoContract.getNomeClienteCartao());
			compraCartao.setNumeroCartao(cartaoContract.getNumeroCartao());
			compraCartao.setDataValidade(cartaoContract.getDataValidade());
			compraCartao.setCodigoSeguranca(cartaoContract.getCodigoSeguranca());
			compraCartao.setCompra(compra);
			compraCartao = cartaoRepository.save(compraCartao);

			//insert na base de Compra Produtos;
			CompraProduto compraProduto = new CompraProduto();
			compraProduto.setIdCompra(compra.getIdCompra());
			compraProduto.setIdProduto(1);
			compraProduto.setValor(carrinho.getValorTotal());
			compraProduto.setCompra(compra);
			compraProduto = produtoRepository.save(compraProduto);

			if (respostaCartao.getCodigoResposta() != 0) {
				return ResponseEntity.badRequest().body(respostaCartao.getMensagemResposta());
			}else {
				return ResponseEntity.ok(compraContract);
			}

		}catch (HttpClientErrorException e) {
			//			System.out.println("Erro especifico" + e.getResponseBodyAsString() + " - " + e.getResponseHeaders() + " - " + e.getStatusText() + " - " + e.getLocalizedMessage() +
			//					" - " + e.getMessage() + " - " + e.getRawStatusCode() + " - " + e.getStatusText());
			return ResponseEntity.badRequest().body(e.getResponseBodyAsString());
		}
		catch(Exception e)

		{
			//			System.out.println("Erro Generico" + e.getMessage());
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
}

