package com.iCommerce.compra.contracts;

public class CompraContract {
	private long idSessao;
	private ClienteContract cliente;
	private CartaoContract cartao;
	
	public void atualizaValorCartao (double valor) {
		this.cartao.setValor(valor);
	}
	
	public long getIdSessao() {
		return idSessao;
	}
	public void setIdSessao(long idSessao) {
		this.idSessao = idSessao;
	}
	public ClienteContract getCliente() {
		return cliente;
	}
	public void setCliente(ClienteContract cliente) {
		this.cliente = cliente;
	}
	public CartaoContract getCartao() {
		return cartao;
	}
	public void setCartao(CartaoContract cartao) {
		this.cartao = cartao;
	}
}
