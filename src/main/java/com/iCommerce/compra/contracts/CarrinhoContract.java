package com.iCommerce.compra.contracts;

import java.util.Date;

public class CarrinhoContract {
	private long idPedido;
	private long idSessao;
	private Date dataCompra;
	private int quantidadeProdutos;
	private String Status;
	private double valorTotal;
	
	public long getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(long idPedido) {
		this.idPedido = idPedido;
	}
	public long getIdSessao() {
		return idSessao;
	}
	public void setIdSessao(long idSessao) {
		this.idSessao = idSessao;
	}
	public Date getDataCompra() {
		return dataCompra;
	}
	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}
	public int getQuantidadeProdutos() {
		return quantidadeProdutos;
	}
	public void setQuantidadeProdutos(int quantidadeProdutos) {
		this.quantidadeProdutos = quantidadeProdutos;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public double getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}
	@Override
	public String toString() {
		return "CarrinhoContract [idPedido=" + idPedido + ", idSessao=" + idSessao + ", dataCompra=" + dataCompra
				+ ", quantidadeProdutos=" + quantidadeProdutos + ", Status=" + Status + ", valorTotal=" + valorTotal
				+ "]";
	}
	
	
	
	
}
