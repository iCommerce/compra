package com.iCommerce.compra.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Compra {
	
	@Id
	private String idCompra;
	private long idPedido;
	private String statusCompra;
	private double valorTotalCompra;
	
	public String getIdCompra() {
		return idCompra;
	}
	public void setIdCompra(String idCompra) {
		this.idCompra = idCompra;
	}
	public long getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(long idPedido) {
		this.idPedido = idPedido;
	}
	public String getStatusCompra() {
		return statusCompra;
	}
	public void setStatusCompra(String statusCompra) {
		this.statusCompra = statusCompra;
	}
	public double getValorTotalCompra() {
		return valorTotalCompra;
	}
	public void setValorTotalCompra(double valorTotalCompra) {
		this.valorTotalCompra = valorTotalCompra;
	}
	
	
	
}
